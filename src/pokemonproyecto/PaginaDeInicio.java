/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonproyecto;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author CltControl
 */
public class PaginaDeInicio {
    private VBox root = new VBox();
    private Label lblInicio = new Label("Bienvenido a Pokemón");
    private Label lblPregunta = new Label("Qué deseas hacer?");
    private HBox paneBotones = new HBox();
    private Button btnCrear = new Button("Crear partida");
    private Button btnCargar = new Button("Cargar partida");
    
    public PaginaDeInicio(){
        organizarPantallaInicio();
        opcionesPartida();
    }
    
    public void organizarPantallaInicio(){
        paneBotones.getChildren().addAll(btnCrear, btnCargar);
        root.getChildren().addAll(lblInicio, lblPregunta, paneBotones);
        
    }
    
    public void opcionesPartida(){
        btnCrear.setOnAction(e-> crearPartida());
        btnCargar.setOnAction(e-> cargarPartida());
    }
    
    public void crearPartida(){
        VBox baseCrear = new VBox();
        HBox paneNombre = new HBox();
        Label lblNombre = new Label("Ingresa tu nombre: ");
        TextField txtNombre = new TextField();
        paneNombre.getChildren().addAll(lblNombre, txtNombre);
        Label lblIngreseGenero = new Label("Escoge tu genero");
        HBox paneGenero = new HBox();
        VBox paneMujer = new VBox();
        VBox paneHombre = new VBox();
        Label lblMujer = new Label("Femenino");
        Label lblHombre = new Label("Masculino");
        Button btnMujer = new Button("F");
        Button btnHombre = new Button("M");
        paneMujer.getChildren().addAll(lblMujer, btnMujer);
        paneHombre.getChildren().addAll(lblHombre, btnHombre);
        paneGenero.getChildren().addAll(paneMujer, paneHombre);
        
        baseCrear.getChildren().addAll(paneNombre,lblIngreseGenero,paneGenero);
        Scene sceneCrear = new Scene(baseCrear);
        Stage crearStage = new Stage();
        crearStage.setScene(sceneCrear);
        crearStage.show();
        
    }
    
    public void cargarPartida(){
        
    }
    
    public VBox getRoot() {
        return root;
    }
    
    
}
