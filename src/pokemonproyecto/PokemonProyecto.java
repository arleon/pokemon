/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonproyecto;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Angellina
 */
public class PokemonProyecto extends Application{

    public static void main(String[] args) {
        deserializarPartida("partida.txt");
        launch();
        serializarPartida("partida.txt");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene1 = new Scene(new PaginaDeInicio().getRoot());
        primaryStage.setScene(scene1);
        primaryStage.show();
    }

    public static void deserializarPartida(String filename) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
            System.out.println("Leido");
        } catch (IOException ex) {
            System.out.println("Problemas con " + ex);
        }
    }

    public static void serializarPartida(String filename) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))) {
            System.out.println("Escrito");
        } catch (IOException ex) {
            System.out.println("Problemas con " + ex);
        }
    }
}
